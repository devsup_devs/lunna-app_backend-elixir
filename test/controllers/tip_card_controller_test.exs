defmodule ArayashikiWs.TipCardControllerTest do
  use ArayashikiWs.ConnCase

  alias ArayashikiWs.TipCard
  @valid_attrs %{collection_method: "some content", locale: "some content", media_link: "some content", optional_link_1: "some content", optional_link_2: "some content", period_cycle: "some content", text: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, tip_card_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing tip cards"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, tip_card_path(conn, :new)
    assert html_response(conn, 200) =~ "New tip card"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, tip_card_path(conn, :create), tip_card: @valid_attrs
    assert redirected_to(conn) == tip_card_path(conn, :index)
    assert Repo.get_by(TipCard, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, tip_card_path(conn, :create), tip_card: @invalid_attrs
    assert html_response(conn, 200) =~ "New tip card"
  end

  test "shows chosen resource", %{conn: conn} do
    tip_card = Repo.insert! %TipCard{}
    conn = get conn, tip_card_path(conn, :show, tip_card)
    assert html_response(conn, 200) =~ "Show tip card"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, tip_card_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    tip_card = Repo.insert! %TipCard{}
    conn = get conn, tip_card_path(conn, :edit, tip_card)
    assert html_response(conn, 200) =~ "Edit tip card"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    tip_card = Repo.insert! %TipCard{}
    conn = put conn, tip_card_path(conn, :update, tip_card), tip_card: @valid_attrs
    assert redirected_to(conn) == tip_card_path(conn, :show, tip_card)
    assert Repo.get_by(TipCard, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    tip_card = Repo.insert! %TipCard{}
    conn = put conn, tip_card_path(conn, :update, tip_card), tip_card: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit tip card"
  end

  test "deletes chosen resource", %{conn: conn} do
    tip_card = Repo.insert! %TipCard{}
    conn = delete conn, tip_card_path(conn, :delete, tip_card)
    assert redirected_to(conn) == tip_card_path(conn, :index)
    refute Repo.get(TipCard, tip_card.id)
  end
end
