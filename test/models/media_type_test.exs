defmodule ArayashikiWs.MediaTypeTest do
  use ArayashikiWs.ModelCase

  alias ArayashikiWs.MediaType

  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = MediaType.changeset(%MediaType{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = MediaType.changeset(%MediaType{}, @invalid_attrs)
    refute changeset.valid?
  end
end
