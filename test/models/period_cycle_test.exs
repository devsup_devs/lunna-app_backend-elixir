defmodule ArayashikiWs.PeriodCycleTest do
  use ArayashikiWs.ModelCase

  alias ArayashikiWs.PeriodCycle

  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = PeriodCycle.changeset(%PeriodCycle{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = PeriodCycle.changeset(%PeriodCycle{}, @invalid_attrs)
    refute changeset.valid?
  end
end
