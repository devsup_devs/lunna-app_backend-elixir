defmodule ArayashikiWs.ColletcionMethodTest do
  use ArayashikiWs.ModelCase

  alias ArayashikiWs.ColletcionMethod

  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ColletcionMethod.changeset(%ColletcionMethod{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ColletcionMethod.changeset(%ColletcionMethod{}, @invalid_attrs)
    refute changeset.valid?
  end
end
