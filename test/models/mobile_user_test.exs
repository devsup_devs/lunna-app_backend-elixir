defmodule ArayashikiWs.MobileUserTest do
  use ArayashikiWs.ModelCase

  alias ArayashikiWs.MobileUser

  @valid_attrs %{email: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = MobileUser.changeset(%MobileUser{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = MobileUser.changeset(%MobileUser{}, @invalid_attrs)
    refute changeset.valid?
  end
end
