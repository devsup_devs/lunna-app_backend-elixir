defmodule ArayashikiWs.TipCardTest do
  use ArayashikiWs.ModelCase

  alias ArayashikiWs.TipCard

  @valid_attrs %{collection_method: "some content", locale: "some content", media_link: "some content", optional_link_1: "some content", optional_link_2: "some content", period_cycle: "some content", text: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = TipCard.changeset(%TipCard{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = TipCard.changeset(%TipCard{}, @invalid_attrs)
    refute changeset.valid?
  end
end
