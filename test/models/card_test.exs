defmodule ArayashikiWs.CardTest do
  use ArayashikiWs.ModelCase

  alias ArayashikiWs.Card

  @valid_attrs %{atributes: %{}, name_en: "some content", name_pt: "some content", options_en: %{}, options_pt: %{}, type_field: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Card.changeset(%Card{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Card.changeset(%Card{}, @invalid_attrs)
    refute changeset.valid?
  end
end
