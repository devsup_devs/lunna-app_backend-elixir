defmodule ArayashikiWs.VisibilityTest do
  use ArayashikiWs.ModelCase

  alias ArayashikiWs.Visibility

  @valid_attrs %{shift_days: 42, show_when: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Visibility.changeset(%Visibility{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Visibility.changeset(%Visibility{}, @invalid_attrs)
    refute changeset.valid?
  end
end
