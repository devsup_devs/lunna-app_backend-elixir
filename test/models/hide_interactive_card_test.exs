defmodule ArayashikiWs.HideInteractiveCardTest do
  use ArayashikiWs.ModelCase

  alias ArayashikiWs.HideInteractiveCard

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = HideInteractiveCard.changeset(%HideInteractiveCard{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = HideInteractiveCard.changeset(%HideInteractiveCard{}, @invalid_attrs)
    refute changeset.valid?
  end
end
