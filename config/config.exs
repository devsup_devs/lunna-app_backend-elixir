# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :guardian, Guardian,
  allowed_algos: ["HS512"], # optional
  verify_module: Guardian.JWT,  # optional
  issuer: "ArayashikiWs",
  ttl: { 30, :days },
  verify_issuer: true, # optional
  secret_key: "jNXdXe309Ym9uXArfh0KppMT39X7jXxNKKk8ElUiic3QPtPc710G9Bng5iBvnERZ",
  serializer: ArayashikiWs.GuardianSerializer

# General application configuration
config :arayashiki_ws,
  ecto_repos: [ArayashikiWs.Repo]

# Configures the endpoint
config :arayashiki_ws, ArayashikiWs.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "jNXdXe309Ym9uXArfh0KppMT39X7jXxNKKk8ElUiic3QPtPc710G9Bng5iBvnERZ",
  render_errors: [view: ArayashikiWs.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ArayashikiWs.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :arayashiki_ws, ArayashikiWs.Gettext, default_locale: "en"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
