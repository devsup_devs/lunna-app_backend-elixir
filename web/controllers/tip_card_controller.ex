defmodule ArayashikiWs.TipCardController do
  use ArayashikiWs.Web, :controller
  alias ArayashikiWs.TipCard

  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  def index(conn, _params) do
    tip_cards = Repo.all from t in TipCard,
                      preload: [:collection_method, :period_cycle, :media_type]
    render(conn, "index.html", tip_cards: tip_cards)
  end

  def new(conn, _params) do
    changeset = TipCard.changeset(%TipCard{})
    media_types = Repo.all(ArayashikiWs.MediaType) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    collection_methods = Repo.all(ArayashikiWs.CollectionMethod) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    period_cycles = Repo.all(ArayashikiWs.PeriodCycle) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    language_options = %{gettext("locale_portuguese") => "pt_BR", gettext("locale_english") => "en"}

    render(conn, "new.html", changeset: changeset, media_types: media_types, language_options: language_options, collection_methods: collection_methods, period_cycles: period_cycles)
  end

  def create(conn, %{"tip_card" => tip_card_params}) do
    changeset = TipCard.changeset(%TipCard{}, tip_card_params)
    media_types = Repo.all(ArayashikiWs.MediaType) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    period_cycles = Repo.all(ArayashikiWs.PeriodCycle) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    collection_methods = Repo.all(ArayashikiWs.CollectionMethod) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})    
    language_options = %{gettext("locale_portuguese") => "pt_BR", gettext("locale_english") => "en"}


    case Repo.insert(changeset) do
      {:ok, _tip_card} ->
        conn
        |> put_flash(:info, "Tip card created successfully.")
        |> redirect(to: tip_card_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, media_types: media_types, language_options: language_options, collection_methods: collection_methods, period_cycles: period_cycles)
    end
  end

  def show(conn, %{"id" => id}) do
    tip_card = Repo.get!(TipCard, id)
    render(conn, "show.html", tip_card: tip_card)
  end

  def edit(conn, %{"id" => id}) do
    tip_card = Repo.get!(TipCard, id)
    changeset = TipCard.changeset(tip_card)
    media_types = Repo.all(ArayashikiWs.MediaType) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    period_cycles = Repo.all(ArayashikiWs.PeriodCycle) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    collection_methods = Repo.all(ArayashikiWs.CollectionMethod) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    language_options = %{gettext("locale_portuguese") => "pt_BR", gettext("locale_english") => "en"}


    render(conn, "edit.html", tip_card: tip_card, changeset: changeset, media_types: media_types, language_options: language_options, collection_methods: collection_methods, period_cycles: period_cycles)
  end

  def update(conn, %{"id" => id, "tip_card" => tip_card_params}) do
    tip_card = Repo.get!(TipCard, id)
    changeset = TipCard.changeset(tip_card, tip_card_params)
    media_types = Repo.all(ArayashikiWs.MediaType) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    period_cycles = Repo.all(ArayashikiWs.PeriodCycle) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    collection_methods = Repo.all(ArayashikiWs.CollectionMethod) |> Enum.map(&{Gettext.gettext(ArayashikiWs.Gettext, Enum.join(["locale_", &1.name], "")), &1.id})
    language_options = %{gettext("locale_portuguese") => "pt_BR", gettext("locale_english") => "en"}

    case Repo.update(changeset) do
      {:ok, tip_card} ->
        conn
        |> put_flash(:info, "Tip card updated successfully.")
        |> redirect(to: tip_card_path(conn, :index))
      {:error, changeset} ->
        render(conn, "edit.html", tip_card: tip_card, changeset: changeset, media_types: media_types, language_options: language_options, collection_methods: collection_methods, period_cycles: period_cycles)
    end
  end

  def delete(conn, %{"id" => id}) do
    tip_card = Repo.get!(TipCard, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(tip_card)

    conn
    |> put_flash(:info, "Tip card deleted successfully.")
    |> redirect(to: tip_card_path(conn, :index))
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_flash(:info, "Authentication required")
    |> redirect(to: "/login")
  end
end
