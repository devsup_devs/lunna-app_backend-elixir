defmodule ArayashikiWs.SessionController do
  use ArayashikiWs.Web, :controller

  plug :put_layout, "session.html"

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, %{"session" => session_params}) do
    case ArayashikiWs.Session.login(session_params, ArayashikiWs.Repo) do
      {:ok, user} ->
        conn
        |> Guardian.Plug.sign_in(user)
        |> put_session(:current_user, user.id)
        |> put_flash(:info, "Logged in")
        |> redirect(to: "/")
      :error ->
        conn
        |> put_flash(:info, "Wrong email or password")
        |> render("new.html")
    end
  end
  
  def delete(conn, _) do
    conn
    |> Guardian.Plug.sign_out
    |> delete_session(:current_user)
    |> put_flash(:info, "Logged out")
    |> redirect(to: "/")
  end
  
  def unauthenticated_api(conn, _params) do
    result_json = %{
      status: 1,
      message: "Authentication required"
    }
    json conn, result_json
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_flash(:info, "Authentication required")
    |> redirect(to: "/login")
  end
end
