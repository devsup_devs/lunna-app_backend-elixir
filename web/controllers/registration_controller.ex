defmodule ArayashikiWs.RegistrationController do
  use ArayashikiWs.Web, :controller
  alias ArayashikiWs.User
  
  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  def new(conn, _params) do
    user = User.changeset(%User{})
    render conn, user: user
  end

  def create(conn, %{"user" => user_params}) do
    user = User.changeset(%User{}, user_params)

    case ArayashikiWs.Registration.create(user, ArayashikiWs.Repo) do
      {:ok, user} ->
        conn
        |> Guardian.Plug.sign_in(user)
        |> put_session(:current_user, user.id)
        |> put_flash(:info, "Your account was created")
        |> redirect(to: "/")
      {:error, user} ->
        conn
        |> put_flash(:info, "Unable to create account")
        |> render("new.html", user: user)
    end
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_flash(:info, "Authentication required")
    |> redirect(to: "/login")
  end
end
