use Timex
defmodule ArayashikiWs.MobileUserController do
  require Logger
  use ArayashikiWs.Web, :controller
  alias ArayashikiWs.MobileUser

  def login_by_email(conn, _params) do
    try do
      mobile_user = Repo.get_by(MobileUser, email: conn.params["email"])
      valid_pass = false
      if (mobile_user != nil) do
        valid_pass = Comeonin.Bcrypt.checkpw(conn.params["password"], mobile_user.crypted_password)
      end
      if (valid_pass) do
        mobile_user = Repo.get(MobileUser, mobile_user.id)
        { :ok, auth_token, full_claims } = Guardian.encode_and_sign(mobile_user, :api)
        result_json = %{
          status: 0,
          message: "success",
          mobile_user_id: mobile_user.id,
          authorization: auth_token
        }
        json conn, result_json
      else
        raise gettext("locale_error_login")
      end
    rescue
      e in RuntimeError -> e
      result_json = %{
        status: 1,
        message: e.message
      }
      json conn, result_json
    end
  end

  def login_by_google(conn, _params) do
    try do
      user_email = conn.params["email"]
      user_google_id = conn.params["google_id"]
      user_name =  conn.params["name"]
      user_last_name =  conn.params["last_name"]
      mobile_user = nil
      if (user_google_id) do
        mobile_user = Repo.get_by(MobileUser, google_id: user_google_id)
      end

      result_json = %{}
      if (mobile_user) do
        mobile_user = Repo.get(MobileUser, mobile_user.id)
        { :ok, auth_token, full_claims } = Guardian.encode_and_sign(mobile_user, :api)
        result_json = %{
          status: 0,
          message: "success",
          mobile_user_id: mobile_user.id,
          authorization: auth_token
        }
      else
        mobile_user = Repo.get_by(MobileUser, email: user_email)

        if (mobile_user) do
          changeset = MobileUser.update_changeset(mobile_user, conn.params)
          { :ok, auth_token, full_claims } = Guardian.encode_and_sign(mobile_user, :api)

          case Repo.update(changeset) do
            {:ok, user} ->
              result_json = %{
                status: 0,
                message: gettext("locale_successful_user_update"),
                mobile_user_id: mobile_user.id,
                authorization: auth_token
              }
            {:error, changeset} ->
              raise gettext("locale_error_user_update")
          end 
        else
          mobile_user_hash = %{
            "name" => user_name,
            "last_name" => user_last_name,
            "email" => user_email, 
            "password" => user_google_id <> user_last_name,
            "google_id" => user_google_id
          }

          mobile_user = MobileUser.changeset(%MobileUser{}, mobile_user_hash)

          case ArayashikiWs.Registration.create(mobile_user, ArayashikiWs.Repo) do
            {:ok, mobile_user} ->
              { :ok, auth_token, full_claims } = Guardian.encode_and_sign(mobile_user, :api)
              result_json = %{
                status: 0,
                message: gettext("locale_successful_user_create"),
                mobile_user_id: mobile_user.id,
                authorization: auth_token
              }
              json conn, result_json     
            {:error, mobile_user} ->
              raise gettext("locale_error_user_create")
          end
          raise gettext("locale_error_user_create")
        end
      end
      json conn, result_json
    rescue
      e in RuntimeError -> e
      result_json = %{
        status: 1,
        message: e.message
      }
      json conn, result_json
    end
  end

  def onboarding_questions(conn, _params) do
    try do
      # Logger.debug "Logging USER MOBILE ID " <> to_string(conn.assigns[:mobile_user_id])
      mobile_user_id = Map.get(Map.get(conn, :assigns), :mobile_user_id)
      first_day_period = conn.params["first_day_period"]
      Logger.debug "Logging USER MOBILE ID " <> mobile_user_id
      
      period_cycle = conn.params["period_cycle"]
      cycle_duration = conn.params["cycle_duration"]

      if(period_cycle == nil) do
        period_cycle = 5
      end

      first_day_period = Timex.parse!(first_day_period, "%Y-%m-%d", :strftime)
      # mobile_user = Repo.get_by(MobileUser, id: mobile_user_id)
      mobile_user = ArayashikiWs.Repo.get(ArayashikiWs.MobileUser, mobile_user_id)

      mobile_user_hash = %{
        "first_day_period" => first_day_period,
        "cycle_duration" => cycle_duration,
        "period_cycle" => period_cycle 
      }

      if (mobile_user) do
        changeset = MobileUser.update_changeset(mobile_user, mobile_user_hash)

        case Repo.update(changeset) do
          {:ok, user} ->
            result_json = %{
              status: 0,
              message: gettext("locale_successful_user_update"),
              mobile_user_id: mobile_user.id
            }
          {:error, changeset} ->
            raise gettext("locale_error_user_update")
        end
      else
        raise gettext("locale_error_user_find")
      end

      json conn, result_json
    rescue
      e in RuntimeError -> e
      result_json = %{
        status: 1,
        message: e.message
      }
      json conn, result_json
    end
  end

  def create_by_email(conn, _params) do
    try do
      mobile_user = MobileUser.changeset(%MobileUser{}, conn.params)

      case ArayashikiWs.Registration.create(mobile_user, ArayashikiWs.Repo) do
        {:ok, mobile_user} ->
          result_json = %{
            status: 0,
            message: gettext("locale_successful_user_create"),
            mobile_user_id: mobile_user.id

          }
          json conn, result_json     
        {:error, mobile_user} ->
          raise gettext("locale_error_user_create")
      end
    rescue
      e in RuntimeError -> e
      result_json = %{
        status: 1,
        message: e.message
      }
      json conn, result_json
    end 
  end
end
