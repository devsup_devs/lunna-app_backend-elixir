use Timex
defmodule ArayashikiWs.CardController do
  use ArayashikiWs.Web, :controller
  alias ArayashikiWs.CardVisibility, as: Visibility
  alias ArayashikiWs.TipCard, as: TipCard
  alias ArayashikiWs.MobileUser
  import Ecto.Query

  plug Guardian.Plug.EnsureAuthenticated, on_failure: { ArayashikiWs.SessionController, :unauthenticated_api }

  def tip_cards(conn, _params) do
    try do
      mobile_user_id = Map.get(Map.get(conn, :assigns), :mobile_user_id)
      mobile_user = Repo.get(MobileUser, mobile_user_id)
      if (mobile_user == nil) do
        raise gettext("locale_error_interactive_card")
      end

      if (mobile_user.collection_method_id == nil) do
        user_collection_method = [1, 2, 3, 4, 5] ++ TipCard.all_collection_method
      else
        user_collection_method = [mobile_user.collection_method_id]
      end

      wish_date = Date.to_string(DateTime.to_date(DateTime.utc_now))
      first_day_period = Ecto.Date.to_string(Ecto.DateTime.to_date(mobile_user.first_day_period))
      period = mobile_user.period_cycle
      cycle_duration = mobile_user.cycle_duration


      user_period_cycle = ArayashikiWs.CalculatorController.menstrual_cycle_algorithm(conn, %{"first_day_period" => first_day_period,
        "period" => period,
        "cycle_duration" => cycle_duration,
        "wish_date" => wish_date})

      user_period_cycle = ArayashikiWs.Repo.get_by(ArayashikiWs.PeriodCycle, name: user_period_cycle["result"]).id

      if (user_period_cycle == nil) do
        raise "Erro ao calcular ciclo"
      end

      locale = Map.get(Map.get(conn, :assigns), :locale)

      collection_method_list = TipCard.all_collection_method ++ user_collection_method
      period_cycle_list = TipCard.all_period_cycle ++ [user_period_cycle]

      query = ArayashikiWs.TipCard |> where([x], x.period_cycle_id in ^period_cycle_list and x.collection_method_id in ^collection_method_list and x.locale == ^locale)

      random_cards = Enum.take_random(ArayashikiWs.Repo.all(query), 3)

      tip_cards = []
      tip_cards = tip_cards ++ Enum.map(random_cards, fn(card) ->
        %{
          title: card.title,
          body_text: card.body_text,
          media_link: card.media_link,
          media_type: card.media_type_id,
          optional_link_1: card.optional_link_1,
          optional_link_2: card.optional_link_2,
          locale: card.locale
        }
      end)

    result_json = %{
      status: 0,
      message: "success",
      tip_cards: tip_cards
    }
    json conn, result_json
    rescue
      e in RuntimeError -> e
      result_json = %{
        status: 1,
        message: e.message
      }
      json conn, result_json
    end
  end

  def response_cards(conn, _params) do
    try do
      result_json = %{
        status: 0,
        message: "success"
      }
      json conn, result_json
    rescue
      e in RuntimeError -> e
      result_json = %{
        status: 1,
        message: e.message
      }
      json conn, result_json
    end
  end

end
