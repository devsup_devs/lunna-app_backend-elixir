use Timex
defmodule ArayashikiWs.CalculatorController do
  alias ArayashikiWs.MobileUser
  use ArayashikiWs.Web, :controller

  plug Guardian.Plug.EnsureAuthenticated, on_failure: { ArayashikiWs.SessionController, :unauthenticated_api }

  def menstrual_cycle_algorithm(conn, %{"first_day_period" => first_day_period,
                                     "period" => period,
                                     "cycle_duration" => cycle_duration,
                                     "wish_date" => wish_date}) do

    first_day_period = Timex.parse!(first_day_period, "%Y-%m-%d", :strftime)
    wish_date = Timex.parse!(wish_date, "%Y-%m-%d", :strftime)

    #Getting the right cycle to calculate
    cond do
      #Backing in time (<-)
      Timex.diff(wish_date, first_day_period, :days) <= 0 ->
        diff_days = Timex.diff(wish_date, first_day_period, :days)
      #Going in time (->)
      Timex.diff(first_day_period, wish_date, :days) <= 0 ->
        diff_days = Timex.diff(wish_date, first_day_period, :days)
    end

    cycles_to_jump = Kernel.trunc(diff_days / cycle_duration)
    first_day_current_period = Timex.shift(first_day_period, days: (cycles_to_jump * cycle_duration))
    #adjust to a previous month, in case that the wish_date is bigger than first_day_current_period
    cond do
      Timex.diff(wish_date, first_day_current_period, :days) < 0 ->
        first_day_current_period = Timex.shift(first_day_current_period, days: (cycle_duration * -1))
      true->
    end

    #Always calculator numbers
    first_day_ovulation_peak = Timex.shift(first_day_current_period, days: 10)
    first_day_post_ovulation = Timex.shift(first_day_current_period, days: 15)
    first_day_next_cycle = Timex.shift(first_day_current_period, days: cycle_duration)
    first_day_pre_menstruation = Timex.shift(first_day_next_cycle, days: -2)
    ovulation_peak_duration = 5

    cond do
      Timex.diff(first_day_current_period, wish_date, :days) <= 0 && Timex.diff(wish_date, first_day_ovulation_peak, :days) < 0 ->
        result = "menstruation"
      Timex.diff(first_day_ovulation_peak, wish_date, :days) <= 0 && Timex.diff(wish_date, first_day_post_ovulation, :days) < 0 ->
        result = "ovulation"
      Timex.diff(first_day_post_ovulation, wish_date, :days) <= 0 && Timex.diff(wish_date, first_day_pre_menstruation, :days) < 0 ->
        result = "post_ovulation"
      Timex.diff(first_day_pre_menstruation, wish_date, :days) <= 0 && Timex.diff(wish_date, first_day_next_cycle, :days) < 0 ->
        result = "pre_menstruation"
      true ->
        result = ""
    end

    # first_day_current_period = Timex.format!(first_day_current_period, "%d-%m-%Y", :strftime)
    # {period,_} = Integer.parse(period)

    %{
      "result" => result,
      "first_day_ovulation_peak" => first_day_ovulation_peak,
      "first_day_post_ovulation" => first_day_post_ovulation,
      "first_day_next_cycle" => first_day_next_cycle,
      "first_day_pre_menstruation" => first_day_pre_menstruation,
      "ovulation_peak_duration" => ovulation_peak_duration,
      "first_day_current_period" => first_day_current_period
    }
  end

  def menstrual_cycle(conn, %{"wish_date" => wish_date}) do
    try do
      mobile_user_id = Map.get(Map.get(conn, :assigns), :mobile_user_id)
      mobile_user = Repo.get(MobileUser, mobile_user_id)
      if (mobile_user == nil or mobile_user.first_day_period == nil or mobile_user.period_cycle == nil or mobile_user.cycle_duration == nil) do
        raise gettext("locale_error_user_data")
      end

      first_day_period = Ecto.DateTime.to_date(mobile_user.first_day_period)
      first_day_period = Ecto.Date.to_string(first_day_period)

      period = mobile_user.period_cycle
      cycle_duration = mobile_user.cycle_duration

      description = ""
      remaining_days = 0

      cycle = menstrual_cycle_algorithm(conn, %{"first_day_period" => first_day_period,
                                       "period" => period,
                                       "cycle_duration" => cycle_duration,
                                       "wish_date" => wish_date})

      result = cycle["result"]
      first_day_ovulation_peak = cycle["first_day_ovulation_peak"]
      first_day_post_ovulation = cycle["first_day_post_ovulation"]
      first_day_next_cycle = cycle["first_day_next_cycle"]
      first_day_pre_menstruation = cycle["first_day_pre_menstruation"]
      ovulation_peak_duration = cycle["ovulation_peak_duration"]
      first_day_current_period = cycle["first_day_current_period"]

      wish_date = Timex.parse!(wish_date, "%Y-%m-%d", :strftime)

      cond do
        result == "menstruation" ->
          remaining_days = period + (Timex.diff(first_day_current_period, wish_date, :days)) - 1
          description = gettext("locale_remaining_days_end_period")
          if remaining_days < 0 do
            remaining_days = (Timex.diff(wish_date, first_day_ovulation_peak, :days)) * -1
            description = gettext("locale_remaining_days_start_ovulation")
          end
        result == "ovulation" ->
          remaining_days = ovulation_peak_duration + (Timex.diff(first_day_ovulation_peak, wish_date, :days)) - 1
          description = gettext("locale_remaining_days_end_ovulation")
        result == "post_ovulation" or result == "pre_menstruation" ->
          remaining_days = (Timex.diff(wish_date, first_day_next_cycle, :days)) * -1
          description = gettext("locale_remaining_days_start_period")
        true ->
          result = ""
      end

      result_json = %{
        "status" => 0,
        "message" => "success",
        "remaining_days" => remaining_days,
        "description" => description
      }
      json conn, result_json
    rescue
      e in RuntimeError -> e
      result_json = %{
        "status" => 1,
        "message" => e.message
      }
      json conn, result_json
    end
  end
end
