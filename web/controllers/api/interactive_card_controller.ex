use Timex
defmodule ArayashikiWs.InteractiveCardController do
  use ArayashikiWs.Web, :controller
  alias ArayashikiWs.CardVisibility, as: Visibility
  alias ArayashikiWs.MobileUser
  import Ecto.Query

  plug Guardian.Plug.EnsureAuthenticated, on_failure: { ArayashikiWs.SessionController, :unauthenticated_api }  

  def interactive_cards(conn, _params) do
    try do
      mobile_user_id = Map.get(Map.get(conn, :assigns), :mobile_user_id)
      mobile_user = Repo.get(MobileUser, mobile_user_id)
      if (mobile_user == nil or mobile_user.first_day_period == nil or mobile_user.period_cycle == nil or mobile_user.cycle_duration == nil) do
        raise gettext("locale_error_interactive_card")
      end
      
      wish_date = Date.to_string(DateTime.to_date(DateTime.utc_now)) 
      first_day_period = Ecto.Date.to_string(Ecto.DateTime.to_date(mobile_user.first_day_period))
      period = mobile_user.period_cycle
      cycle_duration = mobile_user.cycle_duration

      cycle = ArayashikiWs.CalculatorController.menstrual_cycle_algorithm(conn, %{"first_day_period" => first_day_period,
        "period" => period,
        "cycle_duration" => cycle_duration,
        "wish_date" => wish_date})

    today = Timex.parse!(wish_date, "%Y-%m-%d", :strftime)
    diff_after = Timex.diff(cycle["first_day_current_period"], today, :days) + period
    diff_before = Timex.diff(today, cycle["first_day_pre_menstruation"], :days)

    # Everyday
    visibility_list = [Visibility.everyday]

    # menstruation
    if(diff_after > 0 && diff_after <= period) do
      visibility_list = visibility_list ++ [Visibility.period, Visibility.period_and_before_two, Visibility.period_and_before_after_two]
    end

    # before_menstruation
    if(diff_before < 2 && diff_before >= 0) do
      visibility_list = visibility_list ++ [Visibility.period_and_before_two, Visibility.period_and_before_after_two]
    end

    # after_menstruation
    if(diff_after > -2 && diff_after <= 0) do
      visibility_list = visibility_list ++ [Visibility.period_and_before_after_two]
    end

    # first_use
    # TODO
    if(true) do
      visibility_list = visibility_list ++ [Visibility.first_time]
    end

    query_hide_cards = from p in ArayashikiWs.HideInteractiveCard, select: p.interactive_card_id, where: p.mobile_user_id == ^mobile_user.id
    excluded_cards = ArayashikiWs.Repo.all(query_hide_cards)

    query_interactive_cards = from x in ArayashikiWs.InteractiveCard, where: x.visibility_id in ^visibility_list and not(x.id in ^excluded_cards)
    random_cards = Enum.take_random(ArayashikiWs.Repo.all(query_interactive_cards), 2)

    locale = Map.get(Map.get(conn, :assigns), :locale)

    interactive_cards = []
      interactive_cards = interactive_cards ++ Enum.map(random_cards, fn(card) ->
        cond do
          locale == "en" ->
            card_title = card.title_en
            card_options = Map.get(card.options_en, "options")
          locale == "pt" ->
            card_title = card.title_pt
            card_options = Map.get(card.options_pt, "options")
          true ->
            card_title = card.title_pt
            card_options = Map.get(card.options_pt, "options")
        end
        %{
          title: card_title,
          options: card_options,
          type_field: card.type_field,
          type_card: card.type_card,
          attributes: Map.get(card.attributes, "attributes")
        }
      end)


    result_json = %{
      status: 0,
      message: "success",
      interactive_cards: interactive_cards
    }
    json conn, result_json
    rescue
      e in RuntimeError -> e
      result_json = %{
        status: 1,
        message: e.message
      }
      json conn, result_json
    end
  end



  def hide_interactive_card(conn, _params) do
    try do
      mobile_user_id = conn.params["mobile_user_id"]
      interactive_card_id = conn.params["interactive_card_id"]

      query = ArayashikiWs.HideInteractiveCard 
              |> where([x], x.mobile_user_id == ^mobile_user_id and x.interactive_card_id == ^interactive_card_id)
      interactive_card = ArayashikiWs.Repo.all(query)

      if(length(interactive_card) == 0) do
        Repo.insert!(ArayashikiWs.HideInteractiveCard.changeset(%ArayashikiWs.HideInteractiveCard{}, %{mobile_user_id: mobile_user_id, interactive_card_id: interactive_card_id}))
        result_json = %{
          status: 0,
          message: "success"
        }
      else
        result_json = %{
          status: 0,
          message: "Já add"
        }
      end

      json conn, result_json
    rescue
      e in RuntimeError -> e
      result_json = %{
        status: 1,
        message: e.message
      }
      json conn, result_json
    end
  end
end
