use Timex
defmodule ArayashikiWs.PageController do
  use ArayashikiWs.Web, :controller

  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  def index(conn, _params) do
    render conn, "index.html"
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_flash(:info, "Authentication required")
    |> redirect(to: "/login")
  end
end
