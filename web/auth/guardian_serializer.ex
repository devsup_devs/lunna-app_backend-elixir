defmodule ArayashikiWs.GuardianSerializer do
  @behaviour Guardian.Serializer

  alias ArayashikiWs.Repo
  alias ArayashikiWs.User
  alias ArayashikiWs.MobileUser

  def for_token(user = %User{}), do: { :ok, "User:#{user.id}" }
  def for_token(mobile_user = %MobileUser{}), do: { :ok, "MobileUser:#{mobile_user.id}" }
  def for_token(_), do: { :error, "Unknown resource type" }

  def from_token("User:" <> id), do: { :ok, Repo.get(User, id) }
  def from_token("MobileUser:" <> id), do: { :ok, Repo.get(MobileUser, id) }
  def from_token(_), do: { :error, "Unknown resource type" }

end
