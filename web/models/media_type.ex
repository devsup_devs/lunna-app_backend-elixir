defmodule ArayashikiWs.MediaType do
  use ArayashikiWs.Web, :model

  schema "media_types" do
    field :name, :string

    timestamps()
  end


  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
  end
end
