defmodule ArayashikiWs.HideInteractiveCard do
  use ArayashikiWs.Web, :model

  schema "hide_interactive_cards" do
    belongs_to :mobile_user, ArayashikiWs.MobileUser
    belongs_to :interactive_card, ArayashikiWs.InteractiveCard

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:mobile_user_id, :interactive_card_id])
    |> validate_required([:mobile_user_id, :interactive_card_id])
  end
end
