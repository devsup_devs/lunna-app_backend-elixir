defmodule ArayashikiWs.InteractiveCard do
  use ArayashikiWs.Web, :model

  schema "interactive_cards" do
    field :title_pt, :string
    field :title_en, :string
    field :options_pt, :map
    field :options_en, :map
    field :type_field, :string
    field :type_card, :string
    field :attributes, :map
    belongs_to :visibility, ArayashikiWs.Visibility

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title_pt, :title_en, :options_pt, :options_en, :type_field, :type_card, :attributes, :visibility_id])
    |> validate_required([:title_pt, :title_en, :options_pt, :options_en, :type_field, :type_card, :attributes])
  end
end
