defmodule ArayashikiWs.CollectionMethod do
  use ArayashikiWs.Web, :model

  schema "collection_methods" do
    field :name, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
  end
end
