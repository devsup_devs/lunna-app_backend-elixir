defmodule ArayashikiWs.CardVisibility do
  use ArayashikiWs.Web, :model
  @everyday 1
  def everyday, do: @everyday
  @period 2
  def period, do: @period
  @period_and_before_two 3
  def period_and_before_two, do: @period_and_before_two
  @period_and_before_after_two 4
  def period_and_before_after_two, do: @period_and_before_after_two
  @first_time 5
  def first_time, do: @first_time
  @period_and_before_three

  schema "card_visibilities" do
    field :show_when, :string
    field :shift_days, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:show_when, :shift_days])
    |> validate_required([:show_when, :shift_days])
  end
end
