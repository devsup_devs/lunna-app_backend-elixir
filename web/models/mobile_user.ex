defmodule ArayashikiWs.MobileUser do
  use ArayashikiWs.Web, :model

  schema "mobile_users" do
    field :name, :string
    field :last_name, :string
    field :email, :string
    field :crypted_password, :string
    field :google_id, :string
    field :password, :string, virtual: true
    field :first_day_period, Ecto.DateTime
    field :period_cycle, :integer
    field :cycle_duration, :integer
    belongs_to :collection_method, ArayashikiWs.CollectionMethod

    timestamps()
  end

  @required_fields ~w(name last_name email password)
  @optional_fields ~w(google_id first_day_period period_cycle cycle_duration collection_method_id)

  def changeset(struct, params \\ :empty) do
    struct
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:email)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 5)
  end

  def update_changeset(struct, params \\ %{}) do
    required = []
    optional = [
      "email", "name", "last_name", "google_id", "first_day_period", "period_cycle", "cycle_duration", "collection_method_id"
    ]

    struct
    |> cast(params, required, optional)
  end 
end
