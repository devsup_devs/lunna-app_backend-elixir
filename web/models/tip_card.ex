defmodule ArayashikiWs.TipCard do
  use ArayashikiWs.Web, :model
  @all_collection_method [6]
  def all_collection_method, do: @all_collection_method
  @all_period_cycle [5]
  def all_period_cycle, do: @all_period_cycle


  schema "tip_cards" do
    field :title, :string
    field :body_text, :string
    field :media_link, :string
    field :optional_link_1, :string
    field :optional_link_2, :string
    field :locale, :string
    belongs_to :collection_method, ArayashikiWs.CollectionMethod
    belongs_to :period_cycle, ArayashikiWs.PeriodCycle
    belongs_to :media_type, ArayashikiWs.MediaType

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :body_text, :media_link, :optional_link_1, :optional_link_2, :locale, :collection_method_id, :period_cycle_id, :media_type_id])
    |> validate_required([:locale, :collection_method_id, :period_cycle_id])
  end
end
