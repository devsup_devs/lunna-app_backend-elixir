defmodule ArayashikiWs.Headers do
  import Plug.Conn
  
  def init(defaults), do: defaults

  def call(conn, _defaults) do
    conn = fetch_session(conn)
    locale = get_req_header(conn, "locale")
    locale = List.first(locale)
    mobile_user_id = get_req_header(conn, "user")
    mobile_user_id = List.first(mobile_user_id)
    
    cond do
      locale == "pt" || locale == "pt-BR" || locale == "pt_BR" ->
        locale = "pt_BR"
      locale == "en" ->
        locale = "en"
      true ->
        locale = "pt_BR"
    end
    Gettext.put_locale(ArayashikiWs.Gettext, locale)

    conn
    |> assign(:locale, locale)
    |> assign(:mobile_user_id, mobile_user_id)
  end
end

