defmodule ArayashikiWs.Router do
  use ArayashikiWs.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :browser_auth do
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.LoadResource
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug ArayashikiWs.Headers
  end

  pipeline :api_auth do
    plug Guardian.Plug.VerifyHeader 
    plug Guardian.Plug.LoadResource
  end

  scope "/", ArayashikiWs do
    pipe_through [:browser, :browser_auth]

    get "/", PageController, :index
    get "/login",  SessionController, :new
    post "/login",  SessionController, :create
    get "/logout", SessionController, :delete
    resources "/registrations", RegistrationController, only: [:new, :create]
    resources "/tip_cards", TipCardController
  end

  scope "/api", ArayashikiWs do
    pipe_through [:api, :api_auth]

    get "/menstrual_cycle/:wish_date", CalculatorController, :menstrual_cycle

    post "/response_cards", CardController, :response_cards
    get "/tip_cards", CardController, :tip_cards

    get "/interactive_cards", InteractiveCardController, :interactive_cards
    post "/hide_interactive_card", InteractiveCardController, :hide_interactive_card

    post "/onboarding_questions", MobileUserController, :onboarding_questions
    post "/create_login_by_email", MobileUserController, :create_by_email
    post "/login_by_email", MobileUserController, :login_by_email
    post "/login_by_google", MobileUserController, :login_by_google
  end
end
