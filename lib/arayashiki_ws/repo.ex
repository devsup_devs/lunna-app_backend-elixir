defmodule ArayashikiWs.Repo do
  use Ecto.Repo, otp_app: :arayashiki_ws

  @migrations_dir Path.join([:code.priv_dir(:arayashiki_ws), "repo", "migrations"])

  def migrate do
    Ecto.Migrator.run(__MODULE__, @migrations_dir, :up, [all: true])
  end
end
