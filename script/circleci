#!/bin/bash
set -e

# ------------------------------------------------------------------------------
# Export environment variables and set PATH
# ------------------------------------------------------------------------------

export ERLANG_VERSION="18.2.1"
export ELIXIR_VERSION="1.3.1"
export PROJECT_NAME="arayashiki_ws"
export PROJECT_VERSION=0.0.1
export RELEASE_PATH="rel/$PROJECT_NAME/releases/$PROJECT_VERSION/$PROJECT_NAME.tar.gz"

export PATH="$HOME/.asdf/bin:$HOME/.asdf/shims:$PATH"

export MIX_ENV="test"

# ------------------------------------------------------------------------------
# Helper functions
# ------------------------------------------------------------------------------

function install_npm {
  npm install -g npm@3.8.1
}

function install_asdf_and_plugins {
  echo "erlang $ERLANG_VERSION" >> .tool-versions
  echo "elixir $ELIXIR_VERSION" >> .tool-versions

  if [ ! -e $HOME/.asdf ]; then
    git clone https://github.com/HashNuke/asdf.git $HOME/.asdf
  fi

  asdf install

  mix local.hex --force
  mix local.rebar --force
}

function get_deps_and_compile {
  echo "[Setup] Getting deps and compiling"
  cd $HOME/$CIRCLE_PROJECT_REPONAME
  mix do deps.get, deps.compile, compile
}

# ------------------------------------------------------------------------------
# Command handlers
# ------------------------------------------------------------------------------

function setup {
  echo "[Setup] Start - $PROJECT_NAME"
  install_npm
  install_asdf_and_plugins
  get_deps_and_compile
}

function setup_database {
  echo "[Database] Create"
  mix ecto.create

  echo "[Database] Migrate"
  mix ecto.migrate
}

function run_tests {
  mix test
}

function release {
  MESSAGE=$(git log --format=%B -n 1 HEAD)

  if [[ $MESSAGE == *"#release"* ]]; then
    echo "[Release] Creating release"
    mkdir -p priv/static
    mkdir -p rel/

    # --------------------------------------------------------------------------

    # Production
    echo "Release - Production - Making assets, compile and release files"
    brunch build --production
    MIX_ENV=prod mix phoenix.digest && MIX_ENV=prod mix compile && MIX_ENV=prod mix release

    echo "Release - Production - Uploading to DO"
    scp $RELEASE_PATH deploy@162.243.165.122:/home/deploy/

    # --------------------------------------------------------------------------

    # # Staging
    # echo "Release - Staging - Copying prod.secrets.exs"
    # brunch build --production
    # MIX_ENV=prod mix phoenix.digest && MIX_ENV=prod mix compile && MIX_ENV=prod mix release
    #
    # echo "Release - Production - Uploading to DO"
    # # aws s3 cp $RELEASE_PATH $S3_RELEASES_PRODUCTION_PATH/$GIT_HEAD.tar.gz --region=eu-central-1
    # scp $RELEASE_PATH deploy@162.243.165.122:/home/deploy/

  else
    echo "Do not create release"
  fi
}

# ------------------------------------------------------------------------------
# Check argument and delegate to the right function
# ------------------------------------------------------------------------------

case "$1" in
  setup ) setup ;;
  setup_database ) setup_database ;;
  test ) run_tests ;;
  release ) release ;;
esac
