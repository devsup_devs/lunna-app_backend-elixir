# Arayashiki

## Setup

### Install Elixir and Phoenix

Arayashiki is built using [Elixir](https://github.com/elixir-lang/elixir) and
[Phoenix](https://github.com/phoenixframework/phoenix). Use `brew` to install
Elixir:

```bash
$ brew update && brew install elixir
```

Once you have Elixir installed, let's install [Hex](https://hex.pm) and
[Rebar](https://github.com/rebar/rebar) locally:

```bash
$ mix local.hex
$ mix local.rebar
```

To install Phoenix, you can use the following command:

```bash
$ mix archive.install https://github.com/phoenixframework/archives/raw/master/phoenix_new.ez
```

### Mix

Get all dependencies

```bash
$ mix deps.get
```

### Node and npm

Phoenix uses [Brunch](http://brunch.io) for compiling, minifying and combining
assets. Install `npm` (version >= 3.6.0) and `node` (version >= v5.7.0) to use Brunch locally.

```bash
$ brew install npm
$ brew install node
```

Once they are installed you can install the dependencies by running `npm install`
inside the project root directory.

### Database

`Arayashiki` relies on [PostgreSQL](http://www.postgresql.org). If you are using a Mac, install it with brew:

```bash
$ brew install postgres
```

`Arayashiki` uses environmental variables to configure `dev` and `test` databases.
Set `ARAYASHIKI_DATABASE_URL_[MIX_ENV]` env variables to set them up. A good approach
is to declare them in your `~/.bash_profile` or `~/.zshrc`, like:

```bash
export ARAYASHIKI_DATABASE_URL_DEV="postgres://$(whoami)@localhost/arayashiki_ws_dev?pool=10"
export ARAYASHIKI_DATABASE_URL_TEST="postgres://$(whoami)@localhost/arayashiki_ws_test?pool=10"
```

Use [Ecto](https://github.com/elixir-lang/ecto) mix tasks to create and migrate databases:

```bash
$ MIX_ENV=dev mix do ecto.create
$ MIX_ENV=test mix do ecto.create
```

You can run the `seed` import to use data during development:

```bash
$ MIX_ENV=dev mix run priv/repo/seeds.exs
```

This command creates the users:

| Name            | Email                   | Role           | Password |
| --------------- | ----------------------- | -------------- | -------- |
| DeveloperS2     | contato@developerS2.com | Admin          | 12345678 |
| Partner 1       | partner1@admin.com      | Partner        | 12345678 |

Beside users, it also create standard other data.

### Running server (with iEx console):

```bash
$ iex -S mix phoenix.server
```
### i18n and i10n

We use [gettext](https://github.com/elixir-lang/gettext) for internationalization and localization. More information can be found at http://blog.plataformatec.com.br/2016/03/using-gettext-to-internationalize-a-phoenix-application/.

#### Extracting new `gettext` translations
Every time you add a new `gettext` call to the code, you need to extract them to the existend `default.po` file. You can do that by running `mix gettext.extract`.

#### Merging new `gettext` translations
After extracting new translations, you need to merge them to the existent languages. Run `mix gettext.merge priv/gettext` to update them.

#### Extract and merge shortcut
Run `mix extract --merge` to combine the behaviour of the two commands aforementioned.

### Using `ctags`

If you want to use `ctags`, you can generate them using:

```bash
ctags -R --exclude=.git --exclude=web/static/ --exclude=_build --exclude=node_modules --exclude=priv/static/js --exclude=deps .
```

You will need to configure your `ctags` to "understand" Elixir. You can find information about how to do it at [https://github.com/mmorearty/elixir-ctags](https://github.com/mmorearty/elixir-ctags).

## Troubleshooting

### `406` error when running API tests

If you get an error `expected response with status 201, got: 406`, it probably means
you need to rebuild your `plug` dependency. To solve that, execute the following:

```bash
$ mix deps.clean plug --build && mix deps.get
```
