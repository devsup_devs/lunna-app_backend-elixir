defmodule ArayashikiWs.Repo.Migrations.CreateCollectionMethod do
  use Ecto.Migration

  def change do
    create table(:collection_methods) do
      add :name, :string

      timestamps()
    end

  end
end
