defmodule ArayashikiWs.Repo.Migrations.CreateHideInteractiveCard do
  use Ecto.Migration

  def change do
    create table(:hide_interactive_cards) do
      add :mobile_user_id, references(:mobile_users, on_delete: :nothing)
      add :interactive_card_id, references(:interactive_cards, on_delete: :nothing)

      timestamps()
    end
    create index(:hide_interactive_cards, [:mobile_user_id])
    create index(:hide_interactive_cards, [:interactive_card_id])

  end
end
