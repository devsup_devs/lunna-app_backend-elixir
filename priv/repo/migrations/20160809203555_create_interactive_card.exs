defmodule ArayashikiWs.Repo.Migrations.CreateInteractiveCard do
  use Ecto.Migration

  def change do
    create table(:interactive_cards) do
      add :title_pt, :string
      add :title_en, :string
      add :options_pt, :map
      add :options_en, :map
      add :type_field, :string
      add :type_card, :string
      add :attributes, :map
      add :visibility_id, references(:card_visibilities, on_delete: :nothing)

      timestamps()
    end
    create index(:interactive_cards, [:visibility_id])

  end
end
