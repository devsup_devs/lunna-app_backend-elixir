defmodule ArayashikiWs.Repo.Migrations.AddCollectionMethodToMobileUsers do
  use Ecto.Migration

  def change do
    alter table(:mobile_users) do
      add :collection_method_id, references(:collection_methods, on_delete: :nothing)
    end
    create index(:mobile_users, [:collection_method_id])
  end
end
