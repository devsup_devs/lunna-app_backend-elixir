defmodule ArayashikiWs.Repo.Migrations.CreateMobileUser do
  use Ecto.Migration

  def change do
    create table(:mobile_users) do
      add :name, :string
      add :email, :string

      timestamps()
    end

  end
end
