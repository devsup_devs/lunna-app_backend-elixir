alias ArayashikiWs.{Repo, CardVisibility}

defmodule ArayashikiWs.Repo.Migrations.CreateSeedCardVisibility do
  use Ecto.Migration

  def up do
    Repo.insert!(CardVisibility.changeset(%CardVisibility{}, %{show_when: "Everyday", shift_days: 2}))
    Repo.insert!(CardVisibility.changeset(%CardVisibility{}, %{show_when: "Period phase", shift_days: 2}))
    Repo.insert!(CardVisibility.changeset(%CardVisibility{}, %{show_when: "Period phase and before", shift_days: 2}))
    Repo.insert!(CardVisibility.changeset(%CardVisibility{}, %{show_when: "Period phase and before/after", shift_days: 2}))
    Repo.insert!(CardVisibility.changeset(%CardVisibility{}, %{show_when: "First time using the app", shift_days: 2}))
  end
end
