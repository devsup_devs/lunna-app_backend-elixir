defmodule ArayashikiWs.Repo.Migrations.AddsFieldsToMobileUsers do
  use Ecto.Migration

  def change do
    alter table(:mobile_users) do
      add :first_day_period, :datetime
      add :period_cycle, :integer
      add :cycle_duration, :integer
    end
  end
end
