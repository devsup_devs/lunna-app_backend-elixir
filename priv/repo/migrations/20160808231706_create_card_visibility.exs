defmodule ArayashikiWs.Repo.Migrations.CreateCardVisibility do
  use Ecto.Migration

  def change do
    create table(:card_visibilities) do
      add :show_when, :string
      add :shift_days, :integer

      timestamps()
    end

  end
end
