alias ArayashikiWs.{Repo, MediaType}

defmodule ArayashikiWs.Repo.Migrations.CreateSeedMediaTypes do
  use Ecto.Migration

  def change do
    media_type = MediaType.changeset(%MediaType{},
        %{ name: "image" })
    Repo.insert!(media_type)

    media_type = MediaType.changeset(%MediaType{},
        %{ name: "podcast" })
    Repo.insert!(media_type)

    media_type = MediaType.changeset(%MediaType{},
        %{ name: "gif" })
    Repo.insert!(media_type)

    media_type = MediaType.changeset(%MediaType{},
        %{ name: "video" })
    Repo.insert!(media_type)
  end
end
