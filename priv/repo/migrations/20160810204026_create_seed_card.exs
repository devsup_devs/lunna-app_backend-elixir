alias ArayashikiWs.{Repo, InteractiveCard}

defmodule ArayashikiWs.Repo.Migrations.CreateSeedInteractiveCard do
  use Ecto.Migration

  def up do
    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Teve relações sexuais hoje?",
          title_en: "Did you have sex today?",
          options_pt: %{options:
          %{
            list: ["Não", "Sim"],
          }},
          options_en: %{options:
          %{
            list: ["No", "Yes"],
          }},
          type_field: "yes_no",
          type_card: "sex",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 1
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Está sentindo dor de cabeça?",
          title_en: "Are you having headaches?",
          options_pt: %{options:
          %{
            list: ["Não", "Sim"],
          }},
          options_en: %{options:
          %{
            list: ["No", "Yes"],
          }},
          type_field: "yes_no",
          type_card: "headache",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 4
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Está sentindo cólica?",
          title_en: "Are you having cramps today?",
          options_pt: %{options:
          %{
            list: ["Não", "Sim"],
          }},
          options_en: %{options:
          %{
            list: ["No", "Yes"],
          }},
          type_field: "yes_no",
          type_card: "cramps",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 3
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Está sentindo dores nas costas?",
          title_en: "Are you having back pain?",
          options_pt: %{options:
          %{
            list: ["Não", "Sim"],
          }},
          options_en: %{options:
          %{
            list: ["No", "Yes"],
          }},
          type_field: "yes_no",
          type_card: "back_pain",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 3
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Como está o seu fluxo menstrual?",
          title_en: "How is your bleeding flow today?",
          options_pt: %{options:
          %{
            list: ["Sem fluxo", "Tipo borra","Leve", "Médio", "Intenso"],
          }},
          options_en: %{options:
          %{
            list: ["None", "Spotting", "Light", "Medium", "Heavy"],
          }},
          type_field: "slider",
          type_card: "bleeding_flux",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 2
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Descreva como se sente em 3 palavras",
          title_en: "3 words that describe how you feel",
          options_pt: %{options:
          %{
            good_words: ["Energizada(o)", "Positiva(o)", "Otimista(o)", "Forte", "Saudável", "Ótima(o)", "Bem", "Criativa(o)", "Entusiasmada(o)", "Contente", "Feliz", "Alegre", "Forte", "Destemida(o)", "Animada(o)", "Grata(o)", "Orgulhosa(o)", "Satisfeita(o)", "Agradecida(o)", "Empoderada(o)", "Cuidadosa(o)", "Empática(o)", "Amorosa(o)", "Confiável", "Mente aberta"],
            bad_words: ["Nhe", "Podre", "Rough", "Cansada(o)", "Pessimista(o)", "Fraca(o)", "Doente", "Com medo", "Entediada(o)", "Confusa(o)", "Preocupada(o)", "Nervosa(o)", "Inquieta(o)", "Em pânico", "Conflituosa(o)", "Envergonhada(o)", "Desapontada(o)", "Ferida(o)", "Insegura(o)", "Culpada(o)", "Nervosa(o)", "Auto crítica(o)", "Mente fechada", "Ciumenta(o)"]
          }},
          options_en: %{options:
          %{
            good_words: ["Energized", "Positive", "Optimistic", "Strong", "Healthy", "Great", "Good", "Creative", "Enthusiastic", "Glad", "Happy", "Joyful", "Strong", "Encouraged", "Excited", "Grateful", "Proud", "Satisfied", "Thankful", "Balanced", "Caring", "Empathetic", "Loving", "Trusting", "Open-minded"],
            bad_words: ["Meh", "Poor", "Rough", "Tired", "Pessimistic", "Weak", "Sick", "Afraid", "Bored", "Confused", "Concerned", "Nervous", "Restless", "Panicked", "Conflicted", "Ashamed", "Disappointed", "Hurt", "Insecure", "Guilty", "Angry", "Self Critical", "Closed-Minded", "Jealous"]
          }},
          type_field: "hash",
          type_card: "three_words",
          attributes: %{attributes: %{max_choices: 3}},
          visibility_id: 1
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Como você está se sentindo hoje?",
          title_en: "How are you feeling today?",
          options_pt: %{options:
          %{
            list: ["Triste", "Neutra", "Feliz"],
          }},
          options_en: %{options:
          %{
            list: ["Bad", "Neutral", "Good"],
          }},
          type_field: "mood_faces",
          type_card: "mood",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 1
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Qual é o tipo de absorvente menstrual que você utiliza?",
          title_en: "What is your current collection method?",
          options_pt: %{options:
          %{
            list: ["Absorvente Interno", "Absorvente", "Coletor Menstrual", "Protetor Diário", "Esponja Absorvente"],
          }},
          options_en: %{options:
          %{
            list: ["Tampon", "Pad", "Menstrual Cup", "Panty Liner", "Menstrual Sponge"],
          }},
          type_field: "default",
          type_card: "collection_method",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 4
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Você está na sua menstruação?",
          title_en: "Are you already having your period?",
          options_pt: %{options:
          %{
            list: ["Não", "Sim"],
          }},
          options_en: %{options:
          %{
            list: ["No", "Yes"],
          }},
          type_field: "yes_no",
          type_card: "period",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 4
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Sofre com tensões pré-menstruais (TPM)?",
          title_en: "Do you have PMS?",
          options_pt: %{options:
          %{
            list: ["Não", "Sim"],
          }},
          options_en: %{options:
          %{
            list: ["No", "Yes"],
          }},
          type_field: "yes_no",
          type_card: "pm",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 4
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Está com tensão pré-menstrual (TPM)?",
          title_en: "Are you under PMS?",
          options_pt: %{options:
          %{
            list: ["Não", "Sim"],
          }},
          options_en: %{options:
          %{
            list: ["No", "Yes"],
          }},
          type_field: "yes_no",
          type_card: "Under_pms",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 4
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Como está a sua secreção vaginal hoje?",
          title_en: "How is your Cervical Mucus today?",
          options_pt: %{options:
          %{
            list: ["Seca", "Clara de Ovo", "Aguada", "Turva", "Pegajosa"],
          }},
          options_en: %{options:
          %{
            list: ["Dry", "Egg-white", "Watery", "Creamy", "Sticky"],
          }},
          type_field: "default",
          type_card: "cervical_mucus",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 4
          })
    Repo.insert!(interactive_card)

    interactive_card = InteractiveCard.changeset(%InteractiveCard{},
        %{title_pt: "Qual é o seu objetivo atual?",
          title_en: "What’s your current goal?",
          options_pt: %{options:
          %{
            list: ["Evitando gravidez", "Tentando engravidar", "Apenas acompanhando o meu ciclo"],
          }},
          options_en: %{options:
          %{
            list: ["Avoid pregnancy", "Trying to conceive", "Just tracking"],
          }},
          type_field: "default",
          type_card: "current_goal",
          attributes: %{attributes: %{max_choices: 1}},
          visibility_id: 4
          })
    Repo.insert!(interactive_card)
  end
end
