defmodule ArayashikiWs.Repo.Migrations.AddFieldsToMobileUsers do
  use Ecto.Migration

  def change do
    alter table(:mobile_users) do
      add :crypted_password, :string
      add :google_id, :string
      add :last_name, :string
    end
  end
end
