defmodule ArayashikiWs.Repo.Migrations.ChangeFieldsToMobileUsers do
  use Ecto.Migration

  def change do
    create unique_index(:mobile_users, [:email])
  end
end
