alias ArayashikiWs.{Repo, PeriodCycle}
defmodule ArayashikiWs.Repo.Migrations.CreateSeedPeriodCycles do
  use Ecto.Migration

  def change do
    period_cycle = PeriodCycle.changeset(%PeriodCycle{},
        %{ name: "menstruation" })
    Repo.insert!(period_cycle)

    period_cycle = PeriodCycle.changeset(%PeriodCycle{},
        %{ name: "ovulation" })
    Repo.insert!(period_cycle)

    period_cycle = PeriodCycle.changeset(%PeriodCycle{},
        %{ name: "post_ovulation" })
    Repo.insert!(period_cycle)

    period_cycle = PeriodCycle.changeset(%PeriodCycle{},
        %{ name: "pre_menstruation" })
    Repo.insert!(period_cycle)

    period_cycle = PeriodCycle.changeset(%PeriodCycle{},
        %{ name: "all_options" })
    Repo.insert!(period_cycle)
  end
end
