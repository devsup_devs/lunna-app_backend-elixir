defmodule ArayashikiWs.Repo.Migrations.CreateMediaType do
  use Ecto.Migration

  def change do
    create table(:media_types) do
      add :name, :string

      timestamps()
    end

  end
end
