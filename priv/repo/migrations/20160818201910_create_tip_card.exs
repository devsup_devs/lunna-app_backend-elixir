defmodule ArayashikiWs.Repo.Migrations.CreateTipCard do
  use Ecto.Migration

  def change do
    create table(:tip_cards) do
      add :title, :string
      add :body_text, :text
      add :media_link, :string
      add :optional_link_1, :string
      add :optional_link_2, :string
      add :locale, :string
      add :collection_method_id, references(:collection_methods, on_delete: :nothing)
      add :period_cycle_id, references(:period_cycles, on_delete: :nothing)
      add :media_type_id, references(:media_types, on_delete: :nothing)

      timestamps()
    end
    create index(:tip_cards, [:media_type_id, :period_cycle_id, :collection_method_id])

  end
end
