alias ArayashikiWs.{User, Registration, Repo}
defmodule ArayashikiWs.Repo.Migrations.CreateSeedUser do
  use Ecto.Migration

  def up do
    model = User.changeset(%User{}, %{name: "Info", email: "info@developers2.com", password: "12345678"})
    Registration.create(model, ArayashikiWs.Repo)
  end
end
