defmodule ArayashikiWs.Repo.Migrations.CreatePeriodCycle do
  use Ecto.Migration

  def change do
    create table(:period_cycles) do
      add :name, :string

      timestamps()
    end

  end
end
