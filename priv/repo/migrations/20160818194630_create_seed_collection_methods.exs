alias ArayashikiWs.{Repo, CollectionMethod}

defmodule ArayashikiWs.Repo.Migrations.CreateSeedCollectionMethods do
  use Ecto.Migration

  def change do
    collection_method = CollectionMethod.changeset(%CollectionMethod{},
        %{ name: "tampon" })
    Repo.insert!(collection_method)

    collection_method = CollectionMethod.changeset(%CollectionMethod{},
        %{ name: "pad" })
    Repo.insert!(collection_method)

    collection_method = CollectionMethod.changeset(%CollectionMethod{},
        %{ name: "menstrual_cup" })
    Repo.insert!(collection_method)

    collection_method = CollectionMethod.changeset(%CollectionMethod{},
        %{ name: "panty_liner" })
    Repo.insert!(collection_method)

    collection_method = CollectionMethod.changeset(%CollectionMethod{},
        %{ name: "menstrual_sponge" })
    Repo.insert!(collection_method)

    collection_method = CollectionMethod.changeset(%CollectionMethod{},
        %{ name: "all_options" })
    Repo.insert!(collection_method)

  end
end

